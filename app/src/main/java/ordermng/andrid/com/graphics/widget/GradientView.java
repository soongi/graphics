package ordermng.andrid.com.graphics.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import ordermng.andrid.com.graphics.R;

/**
 * Created by 순기 on 2016-10-03.
 */
public class GradientView extends View {
    private Bitmap orgAndroid;
    private static final int PADDING = 20;

    public GradientView(Context context) {
        super(context);
    }

    public GradientView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(Color.BLACK);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.android_h_music);
        orgAndroid = Bitmap.createScaledBitmap(bitmap, 570, 370, false);

    }

    public GradientView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(orgAndroid, 0, 0, null);
        canvas.drawBitmap(orgAndroid, orgAndroid.getWidth() + PADDING, 0, null);
        canvas.save();

        canvas.scale(1.0F, -1.0F);
        canvas.drawBitmap(orgAndroid, orgAndroid.getWidth() + PADDING, -orgAndroid.getHeight() * 2, null);

        Paint paint = new Paint();

        LinearGradient gradient = new LinearGradient((orgAndroid.getWidth() + PADDING) * 2, -orgAndroid.getHeight() - (orgAndroid.getHeight() / 2),
                (orgAndroid.getWidth() + PADDING) * 2, -orgAndroid.getHeight(),
                Color.parseColor("#00FFFFFF"), Color.parseColor("#7FFFFFFF"), Shader.TileMode.MIRROR);
        paint.setShader(gradient);

        canvas.drawRect((orgAndroid.getWidth() + PADDING) * 2, -orgAndroid.getHeight()- (orgAndroid.getHeight() / 2),
                (orgAndroid.getWidth() + PADDING) * 2 + orgAndroid.getWidth(), -orgAndroid.getHeight(),
                paint);

        canvas.restore();
        canvas.drawBitmap(orgAndroid, (orgAndroid.getWidth() + PADDING) * 3, 0, null);

        canvas.drawBitmap(createReflectionImg(orgAndroid), (orgAndroid.getWidth() + PADDING) * 3, orgAndroid.getHeight(), null);


    }

    private Bitmap createReflectionImg(Bitmap src) {
        Bitmap reflectionImg = Bitmap.createBitmap(src.getWidth(), src.getHeight()/2, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(reflectionImg);
        c.save();
        c.scale(1.0F, -1.0F);
        c.drawBitmap(src, 0, -src.getHeight(), null);

        c.restore();
        Paint paint = new Paint();
        LinearGradient shader = new LinearGradient(0, 0, 0, reflectionImg.getHeight(), 0x80ffffff, 0x00ffffff, Shader.TileMode.CLAMP);

        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        c.drawRect(0, 0, reflectionImg.getWidth(), reflectionImg.getHeight(), paint);
        return reflectionImg;
    }
}
