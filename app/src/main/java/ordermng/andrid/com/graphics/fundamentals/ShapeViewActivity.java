package ordermng.andrid.com.graphics.fundamentals;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ordermng.andrid.com.graphics.R;

/**
 * Created by yoons on 2016-09-04.
 */
public class ShapeViewActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shapes);
    }
}
