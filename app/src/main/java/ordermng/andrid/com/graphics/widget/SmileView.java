package ordermng.andrid.com.graphics.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by yoons on 2016-09-10.
 */
public class SmileView extends View {

    private static final String TAG = SmileView.class.getSimpleName();
    //scroll size가 중요하다. 이유는 경우에 따라 스크롤 방향이 달라 보인다.
    private static final int SMILEY_SIZE = 100;
    private static final int PADDING = 20;
    private static final int MIN_COLOR = 0;
    private static final int CNT_SMILE = 100;
    private static final int CANVAS_W = SMILEY_SIZE * (CNT_SMILE - MIN_COLOR) + (CNT_SMILE - 1) * PADDING;
    private static final int CANVAS_H = SMILEY_SIZE * (CNT_SMILE - MIN_COLOR) + (CNT_SMILE - 1) * PADDING;

    public static final int SCROLL_SIZE = 20;

    private int viewX = 0;
    private int viewY = 0;

    //for performance
    private boolean useCopyArea = false;
    private boolean useClip = false;

    int prevVX;
    int prevVY;

    private int screenX = getContext().getResources().getDisplayMetrics().widthPixels;
    private int screenY = getContext().getResources().getDisplayMetrics().heightPixels;

    private int controlHeight;
    private Bitmap bitmap;
    private Canvas newCanvas;
    private Paint paint = new Paint();

    public SmileView(Context context) {
        super(context);
    }

    public SmileView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SmileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public void setControlPanelHeight(int height) {
        controlHeight = height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(CANVAS_W, CANVAS_H);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawSmiles(canvas);
        //drawSmiley(canvas, Color.GREEN,500, 100);
    }

    private void drawSmiles(Canvas canvas) {

        prevVX = viewX;
        prevVY = viewY;
        //G5 ScreenSize 1080 * 1794
        int clipL = 0;
        int clipR = getResources().getDisplayMetrics().widthPixels;
        int clipB = getHeight();
        int clipT = getHeight() - (getResources().getDisplayMetrics().heightPixels - controlHeight);
        Log.d(TAG, "clipL : " + clipL + ", clipR : " + clipR + ", clipT : " + clipT + ", clipB : " + clipB);

        for(int column = 0; column < CNT_SMILE; column++) {
            int x = column * (SMILEY_SIZE + PADDING) + viewX;

            if(useClip) {
                if(x - (SMILEY_SIZE/2 + PADDING) > clipR || x + (SMILEY_SIZE/2 + PADDING) < clipL) {
                    continue;
                }
            }

            for(int row = 0; row < CNT_SMILE; row++) {
                int y = row * (SMILEY_SIZE + PADDING) + viewY;

                if(useClip) {
                    if(y + (SMILEY_SIZE/2 + PADDING) < clipT || y - (SMILEY_SIZE/2 + PADDING) > clipB) {
                        continue;
                    }
                }
                drawSmiley(canvas, Color.rgb(column * 2, row * 2, 0), x, y);
            }
        }
    }


    private void drawSmiley(Canvas canvas, int color, int x, int y) {
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        //draw face
        canvas.drawOval(x, y, x + SMILEY_SIZE, y + SMILEY_SIZE, paint);

        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0F);
        //draw head
        canvas.drawOval(x, y, x + SMILEY_SIZE, y + SMILEY_SIZE, paint);

        //draw smile
        float smileX = x + (int)(SMILEY_SIZE * 0.2F);
        float smileY = y + (int)(SMILEY_SIZE * 0.2F);
        canvas.drawArc(smileX, smileY, smileX + (int)(SMILEY_SIZE * 0.6), smileY + (int)(SMILEY_SIZE * 0.6), 20,140, false, paint);

        //draw eyes
        paint.setStyle(Paint.Style.FILL);
        int eyeSize = Math.max(15, (int)(SMILEY_SIZE * 0.15F));

        float leftEyeX = x + (int)(SMILEY_SIZE * 0.5F) - (int)(SMILEY_SIZE * 0.1F) - eyeSize;
        float leftEyeY = y + (int)(SMILEY_SIZE * 0.3F);
        canvas.drawOval(leftEyeX, leftEyeY, leftEyeX + eyeSize, leftEyeY + eyeSize, paint);

        float rightEyeX = x + (int)(SMILEY_SIZE * 0.5F) + (int)(SMILEY_SIZE * 0.1F);
        float rightEyeY = y + (int)(SMILEY_SIZE * 0.3F);
        canvas.drawOval(rightEyeX, rightEyeY, rightEyeX + eyeSize, rightEyeY + eyeSize, paint);

    }

    public void scroll(int scrollX, int scrollY) {
        viewX += scrollX;
        viewY += scrollY;

        viewX = Math.max(viewX, -1*(CANVAS_W - screenX));
        viewX = Math.min(viewX, 0 );

        viewY = Math.min(viewY, CANVAS_H - (screenY - controlHeight) + SMILEY_SIZE);
        viewY = Math.max(viewY, 0);

        Log.d(TAG, "viewX : " + viewX + ", viewY : " + viewY);
        invalidate();
    }

    public void useClip(boolean use) {
        useClip = use;
    }
    public void useCopy(boolean use) {
        useCopyArea = use;
        //최초는 복사할 필요가 없기 때문에 복사를 하지 않도록 prvVX 값을 의미없는 음수 값으로 설정
        if(useCopyArea) {
            prevVX = Integer.MIN_VALUE;
        }
    }
}
