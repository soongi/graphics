package ordermng.andrid.com.graphics.fundamentals;

import android.app.ActivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import ordermng.andrid.com.graphics.R;

/**
 * Created by yoons on 2016-09-18.
 */
public class SystemInfoActivity extends AppCompatActivity{
    private static final int UNIT = 1024;
    private static final String GIGA = "G";
    private static final String MEGA = "M";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_info);
        setDeviceMemory();
        setTotalHeapMemory();
        setAppMemory();
        setResolution();
    }

    private void setAppMemory() {
        TextView tvAppM = (TextView)findViewById(R.id.tv_app_memory);
        tvAppM.setText(String.valueOf(Runtime.getRuntime().maxMemory() / UNIT / UNIT) + MEGA);
    }

    private void setDeviceMemory() {
        ActivityManager actManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);

        TextView tvDeviceM = (TextView)findViewById(R.id.tv_device_m);
        tvDeviceM.setText(String.valueOf(memInfo.totalMem / UNIT / UNIT) + MEGA);
    }

    private void setTotalHeapMemory() {
        TextView tvTotalHeap = (TextView)findViewById(R.id.tv_total_heap);
        tvTotalHeap.setText(String.valueOf(Runtime.getRuntime().totalMemory() / UNIT / UNIT) + MEGA);
    }

    private void setResolution() {
        TextView tvResolution = (TextView)findViewById(R.id.tv_resolution);
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        tvResolution.setText(width + " x " + height);
    }


}
