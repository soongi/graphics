package ordermng.andrid.com.graphics.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yoons on 2016-09-02.
 */
public class ShapesView extends ImageView {
    private List<Path> shpaes = new ArrayList<>();
    private List<Point> points = new ArrayList<>();
    private boolean getStar = true;

    public ShapesView(Context context) {
        super(context);
    }

    public ShapesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShapesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setAntiAlias(true);

        //paint a gradient sky
        LinearGradient sky = new LinearGradient(0f, 0f, 0, (float)getHeight(), Color.DKGRAY, Color.LTGRAY, Shader.TileMode.MIRROR);
        paint.setShader(sky);

        canvas.drawRect(0f, 0f, getWidth(), 4 * getHeight() / 5, paint);

        //paint a gradient for the ground
        LinearGradient ground = new LinearGradient(0f, 4 * getHeight() / 5f, 0f, (float)getHeight(), Color.BLACK, Color.GRAY, Shader.TileMode.MIRROR);
        paint.setShader(ground);
        canvas.drawRect(0, 4 * getHeight() / 5f, (float) getWidth(), (float)getHeight(), paint);

        for(int i = 0; i < shpaes.size(); i++) {
            Point point = points.get(i);
            int[] colors = {Color.WHITE, Color.GRAY, Color.BLACK};
            //RadialGradient shapeGradient = new RadialGradient(point.x, point.y, (float)point.radius, Color.WHITE, Color.BLACK, Shader.TileMode.MIRROR);
            RadialGradient shapeGradient = new RadialGradient(point.x, point.y, (float)point.radius, colors, null, Shader.TileMode.MIRROR);

            paint.setShader(shapeGradient);
            canvas.drawPath(shpaes.get(i), paint);
        }
    }

    public Path generateStar(double x, double y, double innerRadius, double outerRadius, int pointsCount) {

        Path path = new Path();

        double outerAngleIncrement = 2 * Math.PI / pointsCount;

        double outerAngle = 0.0;
        double innerAngle = outerAngleIncrement / 2.0;

        x += outerRadius;
        y += outerRadius;

        float x1 = (float) (Math.cos(outerAngle) * outerRadius + x);
        float y1 = (float) (Math.sin(outerAngle) * outerRadius + y);

        float x2 = (float) (Math.cos(innerAngle) * innerRadius + x);
        float y2 = (float) (Math.sin(innerAngle) * innerRadius + y);

        path.moveTo(x1, y1);
        path.lineTo(x2, y2);

        outerAngle += outerAngleIncrement;
        innerAngle += outerAngleIncrement;

        for( int i = 1; i < pointsCount; i++) {
            x1 = (float) (Math.cos(outerAngle) * outerRadius + x);
            y1 = (float) (Math.sin(outerAngle) * outerRadius + y);

            path.lineTo(x1, y1);

            x2 = (float) (Math.cos(innerAngle) * innerRadius + x);
            y2 = (float) (Math.sin(innerAngle) * innerRadius + y);

            path.lineTo(x2, y2);

            outerAngle += outerAngleIncrement;
            innerAngle += outerAngleIncrement;
        }

        path.close();
        return path;
    }

    public Path generateDonut(float left, float top, float right, float bottom, Path.Direction direction) {
        Path path = new Path();
        path.addOval(left, top, right, bottom, direction);
        path.close();
        return path;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float centerX = event.getX();
        float centerY = event.getY();

        double innerSize = 30 + (25 * Math.random());
        double outerSize = innerSize + 50 + (15 * Math.random());

        Path newShape;

        if(getStar) {
            int numPoints = (int)(8 * Math.random() + 5);
            newShape = generateStar(centerX, centerY, innerSize, outerSize, numPoints);
            centerX += outerSize;
            centerY += outerSize;
        } else {
            newShape = generateDonut((float)(centerX - outerSize/2), (float)(centerY - outerSize/2),
                    (float)(centerX + outerSize/2), (float)(centerY + outerSize/2), Path.Direction.CW);
        }

        getStar = !getStar;
        shpaes.add(newShape);
        points.add(new Point(centerX, centerY, outerSize * 0.65));
        invalidate();
        return super.onTouchEvent(event);
    }

    class Point {
        float x;
        float y;
        double radius;

        public Point(float x, float y, double radius) {
            this.x = x;
            this.y = y;
            this.radius = radius;
        }
    }
}
