package ordermng.andrid.com.graphics.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.io.IOException;

/**
 * Created by 순기 on 2016-09-24.
 */

public class ScaleView extends View{
    private static final int FULL_SIZE = 600;
    private static final int PADDING = 50;
    private static final int QUAD_SIZE = FULL_SIZE / 2;
    private static final double SCALE_FACTOR = 0.17;

    boolean originalImagePainted = false;
    private static Bitmap originalImage = Bitmap.createBitmap(FULL_SIZE, FULL_SIZE, Bitmap.Config.ARGB_8888);
    private Paint paint = new Paint();
    private Canvas newCanvas = new Canvas(originalImage);

    public ScaleView(Context context) {
        super(context);
    }

    public ScaleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ScaleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(!originalImagePainted) {
            paintOriginalImage(newCanvas);
        }
        drawImage(canvas, 20, false);
        drawImage(canvas, FULL_SIZE + PADDING, true);

        //canvas.drawBitmap(originalImage, 0, 0, null);

    }

    private void paintOriginalImage(Canvas canvas) {
        paint.setColor(Color.BLACK);
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);

        //RGB quadrant
        for(int i = 0; i < QUAD_SIZE; i+=3) {
            int x = i;

            paint.setColor(Color.RED);
            canvas.drawLine(x, 0, x, QUAD_SIZE, paint);

            x++;
            paint.setColor(Color.GREEN);
            canvas.drawLine(x, 0, x, QUAD_SIZE, paint);

            x++;
            paint.setColor(Color.BLUE);
            canvas.drawLine(x, 0, x, QUAD_SIZE, paint);
        }

        //draw picture quadrant
        Bitmap picture = null;
        try {
            picture = BitmapFactory.decodeStream(getContext().getAssets().open("scale.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        canvas.drawBitmap(picture, QUAD_SIZE, 0, null);

        //vector drawing quadrant
        paint.setStrokeWidth(7);
        paint.setColor(Color.WHITE);
        canvas.drawRect(0, QUAD_SIZE, QUAD_SIZE, FULL_SIZE, paint);
        paint.setColor(Color.BLACK);

        paint.setStyle(Paint.Style.STROKE);
        canvas.drawOval(0, QUAD_SIZE, QUAD_SIZE, FULL_SIZE, paint);
        canvas.drawArc(20, QUAD_SIZE + 20, QUAD_SIZE - 20, FULL_SIZE - 20, 20, 140, false, paint);
        paint.setStyle(Paint.Style.FILL);
        int eyeSize = 30;
        int eyePos =  90 - (eyeSize / 2);
        canvas.drawOval(eyePos, QUAD_SIZE + eyePos, eyePos + eyeSize, QUAD_SIZE + eyePos + eyeSize, paint);
        canvas.drawOval(QUAD_SIZE  - eyePos - eyeSize, QUAD_SIZE + eyePos, QUAD_SIZE - eyePos, QUAD_SIZE + eyePos + eyeSize, paint);

        //B&W grid
        paint.setColor(Color.WHITE);
        canvas.drawRect(QUAD_SIZE + 1, QUAD_SIZE + 1, FULL_SIZE, FULL_SIZE, paint);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        for(int i = 0; i < QUAD_SIZE; i+=13) {
            int pos = QUAD_SIZE + i;
            canvas.drawLine(pos, QUAD_SIZE + 1, pos, FULL_SIZE, paint);
            canvas.drawLine(QUAD_SIZE + 1, pos, FULL_SIZE, pos, paint);

        }

        originalImagePainted = true;
    }

    private Bitmap getOptimalScalingImage(Bitmap inputImage, int startSize, int endSize) {
        int currentSize = startSize;
        Bitmap currentImage = inputImage;
        int delta = currentSize - endSize;
        int nextPow2 = currentSize / 2;

        while (currentSize > 1) {
            if (delta <= nextPow2) {
                if (currentSize != endSize) {

                    currentImage = Bitmap.createScaledBitmap(currentImage, endSize, endSize, false);
                }
                return currentImage;
            } else {

                Bitmap tmpImage = Bitmap.createScaledBitmap(currentImage, currentSize /3, currentSize /3, false);
                currentImage = tmpImage;
                currentSize = currentImage.getWidth();
                delta = currentSize - endSize;
                nextPow2 = currentSize/3;
            }
        }
        return currentImage;
    }

    private void drawImage(Canvas canvas, int yLoc, boolean linearWay) {
        int xLoc = 100;
        int delta = (int)(SCALE_FACTOR * FULL_SIZE);


        for(int scaledSize = FULL_SIZE; scaledSize > 0; scaledSize -= delta) {
            Bitmap bitmap = null;
            if(linearWay) {
                bitmap = getOptimalScalingImage(originalImage, FULL_SIZE, scaledSize);
            } else {
                bitmap = Bitmap.createScaledBitmap(originalImage, scaledSize, scaledSize, false);
            }

            canvas.drawBitmap(bitmap, xLoc, yLoc + (FULL_SIZE - scaledSize)/2, paint);
            xLoc = scaledSize + 20 + xLoc;
        }



    }



}
