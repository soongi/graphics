package ordermng.andrid.com.graphics.performance;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import ordermng.andrid.com.graphics.R;
import ordermng.andrid.com.graphics.widget.SmileView;

/**
 * Created by yoons on 2016-09-04.
 */
public class SmileViewActivity extends Activity{
    private static final String TAG = SmileViewActivity.class.getSimpleName();

    private SmileView smileView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smiley);
        smileView = (SmileView)findViewById(R.id.sv_view);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        LinearLayout llControl = (LinearLayout)findViewById(R.id.ll_button);

        smileView.setControlPanelHeight(llControl.getHeight());
        registerHandler();
    }

    private void registerHandler() {
        CheckBox cbClip = (CheckBox)findViewById(R.id.cb_clip);
        cbClip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                smileView.useClip(isChecked);
            }
        });

        CheckBox cbCopy = (CheckBox)findViewById(R.id.cb_copy);
        cbCopy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                smileView.useCopy(isChecked);
            }
        });

        Button btnLeft = (Button)findViewById(R.id.btn_left);
        btnLeft.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                smileView.scroll(SmileView.SCROLL_SIZE, 0);
                return false;
            }
        });

        Button btnRight = (Button)findViewById(R.id.btn_right);
        btnRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                smileView.scroll(-SmileView.SCROLL_SIZE, 0);
                return false;
            }
        });

        Button btnTop = (Button)findViewById(R.id.btn_top);
        btnTop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                smileView.scroll(0, SmileView.SCROLL_SIZE);
                return false;
            }
        });

        Button btnBottom = (Button)findViewById(R.id.btn_bottom);
        btnBottom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                smileView.scroll(0, -SmileView.SCROLL_SIZE);
                return false;
            }
        });
    }
//
//    private void registerHandler() {
//        Button btnLeft = (Button)findViewById(R.id.btn_left);
//        btnLeft.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                smileView.scroll(SmileView.SCROLL_SIZE, 0);
//            }
//        });
//
//        Button btnRight = (Button)findViewById(R.id.btn_right);
//        btnRight.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                smileView.scroll(-SmileView.SCROLL_SIZE, 0);
//            }
//        });
//
//        Button btnTop = (Button)findViewById(R.id.btn_top);
//        btnTop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                smileView.scroll(0, SmileView.SCROLL_SIZE);
//            }
//        });
//
//        Button btnBottom = (Button)findViewById(R.id.btn_bottom);
//        btnBottom.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                smileView.scroll(0, -SmileView.SCROLL_SIZE);
//            }
//        });
//    }
}
