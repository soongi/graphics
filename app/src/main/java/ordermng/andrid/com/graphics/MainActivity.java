package ordermng.andrid.com.graphics;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ordermng.andrid.com.graphics.fundamentals.ShapeViewActivity;
import ordermng.andrid.com.graphics.gradient.GradientActivity;
import ordermng.andrid.com.graphics.performance.ScaleActivity;
import ordermng.andrid.com.graphics.performance.SmileViewActivity;
import ordermng.andrid.com.graphics.fundamentals.SystemInfoActivity;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<String> menus = new ArrayList<>();
        menus.add("Shapes");
        menus.add("Smile");
        menus.add("System");
        menus.add("Scale");
        menus.add("Gradient");

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.menu_row, R.id.tv_title,menus );

        ListView lv = (ListView)findViewById(R.id.lv_menu);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0 :
                startActivity(new Intent(this, ShapeViewActivity.class));
                break;
            case 1:
                startActivity(new Intent(this, SmileViewActivity.class));
                break;
            case 2:
                startActivity(new Intent(this, SystemInfoActivity.class));
                break;
            case 3:
                startActivity(new Intent(this, ScaleActivity.class));
                break;
            case 4:
                startActivity(new Intent(this, GradientActivity.class));
                break;
        }
    }
}
